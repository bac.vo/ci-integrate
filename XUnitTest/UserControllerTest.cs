using System;
using Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Service;
using Web.Controllers;
using Web.Models;
using Xunit;

namespace XUnitTest
{
    /// <summary>
    /// This class is only test result of the controller
    /// </summary>
    public class UserControllerTest : BaseTest
    {
        [Fact]
        public void test_get_user_details()
        {
            var userController = new UserController(userService);
            var result = userController.Details("user1");

            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.True(viewResult.ViewData.Model != null);
            var model = Assert.IsAssignableFrom<User>(viewResult.ViewData.Model);
            Assert.True(model != null);
            Assert.True(model.Wallets != null);
            Assert.True(model.Wallets.Any());
        }

        [Fact]
        public void test_transfer()
        {
            var userController = new UserController(userService);

            var user1 = userService.FindByUsername("user1");
            var user2 = userService.FindByUsername("user2");
            Assert.True(user1 != null & user2 != null);

            userController.CurrentUser = user1;
            var result = userController.Transfer(new TransferBindingModel
            {
                Amount = 10,
                Currency = Currency.Usd,
                UserId = user2.Id
            });
        }
        
    }
}
