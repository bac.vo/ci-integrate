﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace XUnitTest
{
    /// <summary>
    /// This class is only test logic business on service
    /// </summary>
    public class UserServiceTest : BaseTest
    {
        [Theory]
        [InlineData("user1")]
        [InlineData("user2")]
        [InlineData("user3")]
        [InlineData("user11")]
        public void TestFindByUserName(string username)
        {
            var user = userService.FindByUsername(username);
            Assert.True(user != null);
        }

        [Theory]
        [InlineData("user22")]
        public void TestUpdateUser(string username)
        {
            var user = userService.FindByUsername(username);
            Assert.True(user != null);
            user.Address = "Test address";
            userService.Update(user);

            var lastUser = userService.FindByUsername(username);
            Assert.True(!string.IsNullOrEmpty(lastUser.Address) && lastUser.Address.Equals("Test address"));
        }
    }
}
