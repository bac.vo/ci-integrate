﻿using System;
using System.Collections.Generic;
using System.Text;
using Data;
using Microsoft.EntityFrameworkCore;
using Service;
using Service.Interfaces;

namespace XUnitTest
{
    public abstract class BaseTest
    {
        protected ApplicationDbContext DbContext;
        protected IUserService userService;
        protected BaseTest()
        {
            InitContext();
        }

        public void InitContext()
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>().UseInMemoryDatabase("CiDb");
            var context = new ApplicationDbContext(builder.Options);
            InitUserData(context);
            context.SaveChanges();
            DbContext = context;
            userService =new UserService(DbContext);
        }

        private void InitUserData(ApplicationDbContext context)
        {
            var random = new Random();
            for (int i = 0; i < 100; i++)
            {
                var user = new User($"user{i}", $"user{i}@gmail.com");
                user.AddMoney(random.Next(10, 1000), Currency.Usd);
                context.Users.Add(user);
            }
        }
    }
}
