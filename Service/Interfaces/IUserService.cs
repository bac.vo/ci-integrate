﻿using System;
using System.Collections.Generic;
using System.Text;
using Data;

namespace Service.Interfaces
{
    public interface IUserService
    {
        User FindById(string id);
        void Update(User user);
        User FindByUsername(string username);
    }
}
