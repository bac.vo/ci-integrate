﻿using System;
using System.Linq;
using Data;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using Service.Interfaces;

namespace Service
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext _dbContext;
        public UserService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public User FindById(string id)
        {
            return _dbContext.Users
                .Include(x => x.Wallets)
                .FirstOrDefault(x => x.Id == id);
        }

        public User FindByUsername(string username)
        {
            return _dbContext.Users
                .Include(x=>x.Wallets)
                .FirstOrDefault(x => x.UserName == username);
        }

        public void Update(User user)
        {
            _dbContext.Users.Attach(user);
            _dbContext.SaveChanges();
        }


    }
}
