﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Data.Interfaces;

namespace Data
{
    public class User : IEntity<string>
    {
        public User()
        {
            Id = Guid.NewGuid().ToString();
        }

        public User(string username, string email)
        {
            Id = Guid.NewGuid().ToString();
            UserName = username;
            Email = email;
        }

        [MaxLength(50)]
        public string Id { get; set; }
        [MaxLength(100)]
        [Required]
        public string UserName { get; set; }
       
        [MaxLength(300)]
        public string Email { get; set; }

        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        public virtual  ICollection<Wallet> Wallets { get; set; }

        public void AddMoney(decimal amount, Currency currency)
        {
            if (Wallets == null)
                Wallets = new List<Wallet>();
            var wallet = Wallets.FirstOrDefault(x => x.Currency == currency);
            if (wallet == null)
            {
                wallet = new Wallet{Currency = currency,Amount = 0};
                Wallets.Add(wallet);
            }

            wallet.Amount += amount;
        }

        public void SendMoneyToOther(decimal amount, Currency currency, User receiver)
        {
            if (receiver.Wallets == null)
                receiver.Wallets = new List<Wallet>();
            if (Wallets == null)
                throw new Exception("The user does have a wallet amount to send money");
            var wallet = Wallets.FirstOrDefault(x => x.Currency == currency);
            if (wallet == null)
                throw new Exception($"The user does have {currency} wallet. Please try with other currency value.");
            if(wallet.Amount < amount)
                throw new Exception("The amount is not enough to send money. Please try with a lower amount.");
            var receiverWallet = receiver.Wallets.FirstOrDefault(x => x.Currency == currency);
            if (receiverWallet == null)
            {
                receiverWallet = new Wallet { Currency = currency, Amount = 0 };
                receiver.Wallets.Add(receiverWallet);
            }

            wallet.Amount -= amount;
            receiverWallet.Amount += amount;
        }
    }
}
