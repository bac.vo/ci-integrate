﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Data
{
    public class Wallet : IEntity<long>
    {
        public long Id { get; set; }
        public Currency Currency { get; set; }
        public  decimal Amount { get; set; }
        [MaxLength(50)]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
