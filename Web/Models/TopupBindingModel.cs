﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Data;

namespace Web.Models
{
    public class TopupBindingModel
    {
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public Currency Currency { get; set; }
    }
}
