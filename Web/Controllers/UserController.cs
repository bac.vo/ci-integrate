﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data;
using Microsoft.AspNetCore.Mvc;
using Service.Interfaces;
using Web.Models;

namespace Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public User CurrentUser { get; set; }
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public IActionResult Details(string username)
        {
            var user = _userService.FindByUsername(username);
            return View(user);
        }

        [HttpGet]
        public IActionResult Topup()
        {
            var model = new TopupBindingModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Topup(TopupBindingModel model)
        {
            return View();
        }

        [HttpGet]
        public IActionResult Transfer()
        {
            var model = new TransferBindingModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult Transfer(TransferBindingModel model)
        {
            var user = _userService.FindById(CurrentUser.Id);
            var receiver = _userService.FindById(model.UserId);
            if (receiver == null)
                return BadRequest();
            user.SendMoneyToOther(model.Amount, model.Currency, receiver);
            _userService.Update(user);
            _userService.Update(receiver);
            return View();
        }
    }
}
